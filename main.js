let topMenu = document.querySelector('.top-menu');
let footerMenu = document.querySelector('.footer-menu');

window.onload = function() {
    if (localStorage.getItem('colorTopMenu') !== null && localStorage.getItem('colorFooterMenu') !== null) {
        let colorTopMenu = localStorage.getItem('colorTopMenu');
        let colorFooterMenu = localStorage.getItem('colorFooterMenu');
        topMenu.classList.add(`${colorTopMenu}`);
        footerMenu.classList.add(`${colorFooterMenu}`);

    }
    document.querySelector('.btn').onclick = function () {
        topMenu.classList.toggle('change-top');
        footerMenu.classList.toggle('change-footer');

     if(topMenu.classList.contains('change-top') && footerMenu.classList.contains('change-footer')){
         localStorage.setItem('colorTopMenu', 'change-top');
         localStorage.setItem('colorFooterMenu', 'change-footer');
     }else{
         localStorage.clear();
     }
   };
};